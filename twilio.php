<?php

/**
 * Plugin Name: Twilio for iStep
 * Plugin URI: https://bitbucket.org/SinOB/twilio_for_wordpress
 * Description: ALlow iStep members to text in their step counts. Incomplete.
 * Very heavily based on code from the 'Twilio for Wordpress' plugin.
 * Version: 0.1-alpha
 * Author: Sinead O'Brien
 * Author URI: https://bitbucket.org/SinOB
 * Requires at least: WP 3.9, Innovage_pedometer
 * Tested up to: WP 4.0.1
 * License: GNU General Public License 2.0 (GPL) http://www.gnu.org/licenses/gpl.html
 * 
 * 
 * Original Plugin Name: Twilio for WordPress
 * Original Plugin URI: http://marcusbattle.com/plugins/twilio-for-wordpress
 * Original Description: Allows developers to extend the Twilio API into WordPress and build exciting communication based themes and plugins. Comes with SMS support to text users from your themes or plugins. VoIP coming soon.
 * Original Author: Marcus Battle
 * Original Author URI: http://marcusbattle.com/plugins 
 */

include_once('inc/twilio-php/Services/Twilio.php');


/* TODO
 * Build the tool to check the auth key is correct
 * Build the tool to get the wordpress user id
 * Make sure the start date is valid
 * Make sure the end date is valid
 * Make sure the step count is not too high
 * Make sure the auth key is a valid format (not just authentic)
 * Make sure the tool will update the step count for the user
 * 
 * Decide how we are going to handle - lock to specific mobile number
 * or lock to unique key
 */

/*
 * This plugin is based on the plugin 'Twilio for WordPress' by Marcus Battle.
 * Huge sections of his code have been removed as they were largly unnecessary.
 * What remains has been heavily customised to allow us to update the custom
 * table wp_innovage_steps with a users steps.
 * 
 * Input is expected to be received in the format
 * <start date in DD/MM/YYYY> <end date in format DD/MM/YYYY> 
 * <step count with no spaces or special characters> <unique_user_key no spaces>
 *  */

/**
 * Sends a standard text message to the supplied phone number
 * @param $to | Recipient of sms message
 * @param $message | Message to recipient
 * @param $from | Twilio number for WordPress to send message from
 * @return array | $response
 * @since 0.1.0
 */
function twilio_send_sms($to, $message, $from = '') {

    $AccountSID = get_option('twilio_account_sid', '');
    $AuthToken = get_option('twilio_auth_token', '');

    if (empty($from)) {
        $from = get_option('twilio_number', '');
    }

    $client = new Services_Twilio($AccountSID, $AuthToken);

    $response = $client->account->messages->sendMessage(
            $from, // From a valid Twilio number
            $to, // Text this number
            $message
    );

    $decoded_response = json_decode($response);

    return $decoded_response;
}

/**
 * Builds the Twilio settings menus 
 * @since 0.1.0
 */
function twilio_admin_menu() {
    add_options_page('Twilio', 'Twilio for iStep', 'manage_options', 'twilio', 'twilio_page_settings');
}

add_action('admin_menu', 'twilio_admin_menu');

/**
 * Displays the 'Home' page in settings
 * @since 0.1.0
 */
function twilio_page_settings() {
    include_once( 'pages/settings.php' );
}

/**
 * Saves the settings from the options pages for Twilio
 * @since 0.1.0
 */
function twilio_page_save_settings() {

    if (isset($_GET['action']) && ( $_GET['action'] == 'update' )) {

        if ($_GET['page'] == 'twilio') {

            update_option('twilio_account_sid', $_GET['accountSID']);
            update_option('twilio_auth_token', $_GET['authToken']);
            update_option('twilio_number', $_GET['twilio_number']);
        }

        // Redirect back to settings page after processing
        $goback = add_query_arg('settings-updated', 'true', wp_get_referer());
        wp_redirect($goback);
    }
}

add_action('init', 'twilio_page_save_settings');

/**
 * Setups up routing to process the callbacks from Twilio
 * @since 0.1.0
 */
function twilio_callbacks() {

    // Parse the uri
    $uri = parse_url($_SERVER['REQUEST_URI']);
    $uri['path'] = str_replace(home_url('', 'relative'), '', $uri['path']);
    $uri['path'] = trailingslashit($uri['path']);

    // Callback for Twilio SMS
    if ($uri['path'] == '/twilio/sms/') {

        $sms = $_REQUEST;
        apply_filters('twilio_sms_callback', $sms);

        exit;
    }
}

add_action('init', 'twilio_callbacks');

/** /
 * Parse the sms and update the user step count if the data is in the 
 * correct format
 *
 * @param type $sms
 * @return null
 */
function twilio_istep_parse_sms($sms) {
    $data = array(
        'start_date' => null,
        'end_date' => null,
        'step_cnt' => null,
        'user_id' => null
    );

    $txt = isset($sms['Body']) ? $sms['Body'] : '';
    //todo clear up the content before split
    //$txt = preg_replace("/[^A-Za-z0-9]/u", " ", $txt);
    $txt = trim($txt);
    $txt = strtolower($txt);
    $msg_parts = explode(' ', $txt);
    if (count($msg_parts) < 4) {
        twilio_istep_parse_error('Insufficient information provided.', $sms['From']);
        return;
    } else if (count($msg_parts) > 4) {
        twilio_istep_parse_error('Too much information provided.', $sms['From']);
        return;
    }

    $data['start_date'] = $msg_parts[0];
    $data['end_date'] = $msg_parts[1];
    $data['step_cnt'] = $msg_parts[2];
    $data['user_key'] = $msg_parts[3];
    $data['user_id'] = null;

    $start_date = new DateTime($data['start_date']);
    $end_date = new DateTime($data['end_date']);
    $current_date = new DateTime();

    if ($start_date > $current_date) {
        twilio_istep_parse_error('The start date cannot be in the future!', $sms['From']);
        return;
    }
    if ($end_date > $current_date) {
        twilio_istep_parse_error('The end date cannot be in the future!', $sms['From']);
        return;
    }

    // If the start date is not formatted corerctly report error and die
    if (!preg_match('/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/', $data['start_date'])) {
        twilio_istep_parse_error('The start date must be in the format DD/MM/YY.', $sms['From']);
        return;
    }

    // If the end date is not formatted corerctly report error and die
    if (!preg_match('/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/', $data['end_date'])) {
        twilio_istep_parse_error('The end date must be inthe format DD/MM/YY.', $sms['From']);
        return;
    }

    // Sanatise the step count and resport error if unable to resolve
    $data['step_cnt'] = preg_replace("/[^A-Za-z0-9]/u", " ", $data['step_cnt']);
    if (!intval($data['step_cnt'])) {
        twilio_istep_parse_error('The step count must be a number with no spaces or special characters.', $sms['From']);
        return;
    }

    // Validate that the user key is correct
    if (!twilio_istep_is_valid_user_key($data['user_key'])) {
        twilio_istep_parse_error('The user key is missing or invalid.', $sms['From']);
        return;
    }

    $data['user_id'] = twilio_istep_get_user_id($data['user_key']);
    // Get the wordpress guid for the user
    twilio_istep_update_pedometer($data);
}

/** /
 * Handle an error in the received sms format.
 * Sends a response informing the user there was an issue
 *
 * @param type $error
 * @param type $reply_to_from
 */
function twilio_istep_parse_error($error, $reply_to_from) {
    $site_name = get_bloginfo('name');
    $message = 'Error processing your txt message to ' . $site_name . '. ' . $error;
    twilio_send_sms($reply_to_from, $message, $from = '');
}

/** /
 * Check that the auth key supplied is valid
 *
 * @param type $key
 * @return boolean
 */
function twilio_istep_is_valid_user_key($key) {
    // NB todo build this
    error_log('Build checker for valid user key');
    return true;
}

/** /
 * Get the wordpress user_id based on the auth key supplied
 *
 * @param type $key
 * @return int
 */
function twilio_istep_get_user_id($key) {
    // NB todo build this
    error_log('Build getter for user id');
    return 1;
}

/** /
 * Calculate the step count for each day in the range and update the
 * step table with the appropriate information
 * @param type $data
 * @return type
 */
function twilio_istep_update_pedometer($data) {

    if (!function_exists('innovage_pedometer_update_steps')) {
        return;
    }

    if (!function_exists('innovage_pedometer_get_dates_interval')) {
        return;
    }

    // Get count of days between start and end date
    $start_date = new DateTime($data['start_date']);
    $end_date = new DateTime($data['end_date']);

    $interval = innovage_pedometer_get_dates_interval($start_date, $end_date);

    // Divide total steps by number of days and convert from float to int
    $steps_per_day = intval($data['step_cnt'] / $interval);

    // Quick hack to include the end date in the period
    $end_date->modify('+1 second');

    // For each date add those steps to the database
    $day_interval = DateInterval::createFromDateString('1 day');
    $period = new DatePeriod($start_date, $day_interval, $end_date);

    foreach ($period as $date) {
        innovage_pedometer_update_steps($user_id, $date, $steps_per_day);
    }
}

add_filter('twilio_sms_callback', 'twilio_istep_parse_sms', 98, 2);
?>