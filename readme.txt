=== Innovage Pedometer ===
Contributors: SinOB
Tags: twilio
Requires at least: 4.0
Tested up to: 4.0.1
Stable tag: 0.1-alpha
License: GPLv2
License URI: http://www.gnu.org/licenses/gpl-2.0.html


Allow users to text in the number of steps they have walked and update the site
accordingly. In alpha only as told to stop working on.


== Description ==

Hoped to eventually build the plugin to allow site members to text in the number 
of steps they have walked. This was put on hold until the practical application 
could be proven.

Very heavily based on plugin 'Twilio For Wordpress' by Marcus Battle
http://marcusbattle.com/plugins/twilio-for-wordpress

Got as far as creating a function (twilio_istep_parse_sms) to parse trusted input.
Expects to receive text in the following format:
"<start_date in DD/MM/YY> <end_date in DD/MM/YY> <step count as int> <site user id>"

Requires innovage_pedometer plugin to be installed and active.

Admin options are available to set the information for your Twilio account


== Installation ==

1. Download
2. Upload to your '/wp-contents/plugins/' directory.
3. Activate the plugin through the 'Plugins' menu in WordPress.
